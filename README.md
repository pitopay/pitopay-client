Project contains the following features:

- **Bootstrap** and **jQuery** installed
- Hot Module Replacement (**HMR**)
- Support **ES6** Syntax (**Babel 7**)
- **ESLint** + **Prettier** = **Neat Coding Style**
- Webpack production building (**code splitting**, **cache**, **lazy-loading** and [**Terser**](https://github.com/terser-js/terser))  

### Quick Start

Install all packages and dependencies required for this project:

    npm install
    
Start the development environment (then, navigate to https://api.pitopay.com):

    npm run dev
 
Then, open a browser and navigate to: https://api.pitopay.com/ 
    
Building files can be done as follows:

    npm run build
    
Deploy to `gh-pages` branch on GitHub.    

    npm run deploy
