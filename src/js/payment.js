const code = localStorage.getItem('paymentLink');

let interval = null;

if (!code) {
  // window.location.replace('/index.html');
} else {
  startTransaction();
}

$(document).ready(function() {
  $('#success-body-card').css('display', 'none');
  $('#pay-body-card').css('display', 'block');
});

function startTransaction() {
  fetch(`https://api.pitopay.com/check/${code || 'a'}`, { method: 'POST' })
    .then(res => res.json())
    .then(checkData => {
      if (checkData.statusCode === 404 && checkData.entity === 'code') {
        document.write('Not Found');
        localStorage.clear();
      }

      fetch(`https://api.pitopay.com/pay/${code}`, { method: 'POST' })
        .then(rs => rs.json())
        .then(data => {
          if (data.statusCode === 200) {
            $('#payment-link').text(data.data.address);
            $('#payment-amount-pi').text(data.data.pi);

            localStorage.setItem('secretKey', data.data.paymentLink);

            interval = setInterval(checkPaymentInterval, 10000);
          }
        });
    });
}

setInterval(checkPaymentInterval, 10000);

function checkPaymentInterval() {
  fetch(`https://api.pitopay.com/status/${localStorage.getItem('secretKey')}`, { method: 'POST' })
    .then(res => res.json())
    .then(data => {
      if (data.isPaid) {
        $('#pay-body-card').css('display', 'none');
        $('#success-body-card').css('display', 'block');
      }
    });
}
