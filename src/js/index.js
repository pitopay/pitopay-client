import 'bootstrap';
import 'bootstrap-select';
import queryString from 'query-string';

import './payment';

const parsed = queryString.parse(location.search);

if (parsed.code) {
  localStorage.setItem('paymentLink', parsed.code);

  window.location.replace('/payment.html');
}

var ProgressBar = require('progressbar.js');

import '../scss/index.scss';

// loading
function onReady(callback) {
  var intervalID = window.setInterval(checkReady, 1000);

  function checkReady() {
    if (document.getElementsByTagName('body')[0] !== undefined) {
      window.clearInterval(intervalID);
      callback.call(this);
    }
  }
}

function show(id, value) {
  document.getElementById(id).style.display = value ? 'block' : 'none';
}

onReady(function() {
  show('page', true);
  show('loading', false);
});

// dropdown
$('.selectpicker').selectpicker();

// code highlight js
// $(document).ready(function() {
//   hljs.initHighlightingOnLoad();
// });

// copy
(function() {
  new ClipboardJS('.btn-copy');
  new ClipboardJS('.btn-copy-link');
  new ClipboardJS('.btn-payment-copy');
  new ClipboardJS('.btn-trs-copy');
})();

// popover
$(function() {
  $('[data-toggle="popover"]').popover();
});

$('.pop').popover().click(function() {
  setTimeout(function() {
    $('.pop').popover('hide');
  }, 1000);
});

// timer
function startTimer(duration, display) {
  var timer = duration, minutes, seconds;
  var handle = setInterval(function() {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    display.textContent = minutes + ':' + seconds;

    if (--timer < 0) {
      timer = duration;
    }
  }, 1000);

  setTimeout(function() {
    clearInterval(handle);
  }, 901000);
}

window.onload = function() {
  var Minutes = 60 * 15,
    display = document.querySelector('#time');
  if (display) {
    startTimer(Minutes, display);
  }
};

// progress bar
if (document.querySelector('#container')) {
  var bar = new ProgressBar.Circle(container, {
    strokeWidth: 5,
    easing: 'easeInOut',
    duration: 900000,
    color: '#fff',
    trailColor: '#eef4ff',
    trailWidth: 1,
    svgStyle: null,
  });

  bar.animate(1.0);
}

// scroll
$(document).ready(function() {
  $('#form-text-danger').css('display', 'none');
  $('#form-text-danger-all').css('display', 'none');
  var scrollLink = $('.scroll');
  scrollLink.click(function(e) {
    e.preventDefault();
    $('body,html').animate(
      {
        scrollTop: $(this.hash).offset().top,
      },
      1000,
    );
  });
});

// create code
$(document).ready(function() {
  // first hide loading and code, after click and create show code
  $('.loading-panel-code').hide();
  $('.code-show').hide();

  $('.create-code').click(function() {
    $('#form-text-danger').text('');
    $('#form-text-danger').css('display', 'none');
    $('#form-text-danger-all').css('display', 'none');

    const amount = $('#amount-button-payment').val();
    const currency = $('#select-button-payment').val();
    const address = $('#address-button-payment').val();

    if (amount === '' || address === '') {
      $('#form-text-danger-all').text('Please fill out the fields.');
      $('#form-text-danger-all').css('display', 'block');
    } else {
      $('.loading-panel-code').show();
      $('.pre-code').hide();

      fetch('https://api.pitopay.com/payment', {
        method: 'POST',
        body: JSON.stringify({
          amount,
          address,
          currency,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(res => res.json())
        .then(data => {
          if (data.statusCode === 400 && data.entity === 'address') {
            $('#form-text-danger-all').text(data.message);
            $('#form-text-danger-all').css('display', 'block');
          } else if (data.statusCode === 500) {
            $('#form-text-danger-all').text('Internal server error');
            $('#form-text-danger-all').css('display', 'block');
          } else if (data.statusCode === 200) {
            $('.code-show').text(`
<button style="background-color: #0067e7; padding: 10px 20px; border-radius: 4px;border: solid 1px #ebedf1">
  <a href="https://pitopay.com/?code=${data.data._id}" style="color: white; text-decoration: none;">CLICK HERE</a>
</button>
            `);

            $('.loading-panel-code').hide();
            $('.pre-code').show();
            $('.code-show').show();
            $('.not-defined-code').hide();
          }
        });
    }
  });
});

// create link
$(document).ready(function() {
  $('#form-text-danger-link').css('display', 'none');
  $('#form-text-danger-all-link').css('display', 'none');

  $('.loading-panel').hide();

  $('.create-link').click(function() {
    $('#form-text-danger-link').text('');
    $('#form-text-danger-link').css('display', 'none');
    $('#form-text-danger-all-link').css('display', 'none');

    const amount = $('#amount-link-payment').val();
    const currency = $('#select-link-payment').val();
    const address = $('#address-link-payment').val();

    if (amount === '' || address === '') {
      $('#form-text-danger-all-link').text('Please fill out the fields.');
      $('#form-text-danger-all-link').css('display', 'block');
    } else {
      $('.loading-panel').show();
      $('.card-copy-link').hide();

      fetch('https://api.pitopay.com/payment', {
        method: 'POST',
        body: JSON.stringify({
          amount,
          address,
          currency,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(res => res.json())
        .then(data => {
          if (data.statusCode === 400 && data.entity === 'address') {
            $('#form-text-danger-all-link').text(data.message);
            $('#form-text-danger-all-link').css('display', 'block');
          } else if (data.statusCode === 500) {
            $('#form-text-danger-all-link').text('Internal server error');
            $('#form-text-danger-all-link').css('display', 'block');
          } else if (data.statusCode === 200) {
            $('.loading-panel').hide();
            $('.card-copy-link').show();
            $('#link').html(`https://pitopay.com/?code=${data.data._id}`);
          }
        });
    }
  });
});
